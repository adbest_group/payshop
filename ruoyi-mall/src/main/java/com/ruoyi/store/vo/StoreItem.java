package com.ruoyi.store.vo;

/**
 * Created by 魔金商城 on 17/11/23.
 * 店铺查询条件枚举
 */
public enum StoreItem {
    SKUNUM, NEWSKUNUM, MARKETSKUNUM, BRAND, ATTENNUM, SALENUM
}
